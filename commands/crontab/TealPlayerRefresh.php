<?php
namespace app\commands\crontab;

use app\modules\common\services\EnumService;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\services\DataTodoService;
use app\modules\data\services\UpdateConfigService;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\services\QueueServer;
use app\modules\task\services\standardIncrement\StandardCommon;

class TealPlayerRefresh extends CrontabBase implements CrontabInterface
{
    private $gameId;
    public function run($args)
    {
        $this->runModel=$args['run_model'];
        $this->gameId=$args['game_id']?$args['game_id']:'all';
        $this->log(sprintf("start run with model:%s,game_id:%s",$this->runModel,$this->gameId));
        for($i=1;$i<9999999;$i++){
            $list=$this->getTeamList($this->gameId,$i);
            if(!$list){
                $this->log("nothing to run ");
                return ;
            }
            foreach ($list as $key=>$val){
                $needRefresh=$this->checkWithTeam($val);
                if($needRefresh){
                    // 添加到todo
                    $this->refreshTeamPlayerRel($val);
                }
            }
            return;
        }
    }

    public function refreshTeamPlayerRel($standardTeamId)
    {
        // 查询standard ,插入变化事件，触发变化任务
        $stdInfo = StandardDataTeam::find()->where(['id'=>$standardTeamId])->asArray()->one();
        $originTypeInfo = $this->getOriginInfoById($stdInfo['origin_id']);
        $resourceType = 'team';
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
            $originTypeInfo['name'],
            $resourceType,
            "change",
            "");
        $changeBody = [
            'changed'=>true,
            'change_type'=>'change',
            'diff'=>[
                'players'=>[
                    'key'=>'players',
                    'before'=>$stdInfo['players'],
                    'after'=>$stdInfo['players']
                ],
            ],
            "old"=>$stdInfo,
            "new"=>$stdInfo
        ];
        $item = [
            "tag" => $tag,
            "params" => $changeBody,
        ];

        $relInfo=DataStandardMasterRelation::find()->where(["standard_id"=>$standardTeamId,'resource_type'=>'team'])->one();
        $majorId=$relInfo["master_id"];
        if(!$majorId){
            return [];
        }
        // 获取对应资源的更新配置
        $updateConfig=UpdateConfigService::getResourceUpdateConfig($majorId,$resourceType);
        $diff=$changeBody['diff'];
        // 添加到todo
        StandardCommon::addTodoByDiffInfo($stdInfo, $resourceType, $diff, 0, $forceUpdateStatus=false);
        print_r($item);
        exit;
    }

    public function getOriginInfoById($originId)
    {
        $infoList=EnumService::getEnum('origin');
        return $infoList[$originId];
    }

    public function getTeamList($gameId,$page=1,$perPage=500)
    {
        $sqlList=<<<SQL
select stdtm.id as standard_id, stdtm.players as standard_players, tm.id as master_id
from standard_data_team stdtm
         left join data_standard_master_relation rel on stdtm.id = rel.standard_id and rel.resource_type = 'team'
         left join team tm on rel.master_id = tm.id
where stdtm.players <> "" and stdtm.players <> "[]"
  and stdtm.deal_status = 4
SQL;
        $andWhere="";
        if($gameId<>'all'){
            $andWhere=" and stdtm.game_id=".$gameId;
        }
        $sqlList.=$andWhere;
        $sqlList.=sprintf(" order by stdtm.id limit %s offset %s ",$perPage,$perPage*($page-1));
        $list=\Yii::$app->getDb()->createCommand($sqlList)->queryAll();
        return $list;
    }

    public function checkWithTeam($teamInfo)
    {
        $sqlTpl=<<<SQL
select stdpl.id as std_player_id,tm.id as team_id
from standard_data_player stdpl
         left join data_standard_master_relation rel on stdpl.id = rel.standard_id and rel.resource_type = 'player'
         left join player pl on rel.master_id = pl.id
         left join team_player_relation trel on pl.id = trel.player_id
         left join team tm on trel.team_id = tm.id 
where stdpl.id in (%s)
SQL;
        $standardTeamId=$teamInfo['standard_id'];
        $masterTeamId=$teamInfo['master_id'];
        $standardPlayers=$teamInfo['standard_players'];
        $this->log(sprintf("start run with standard_team :%s,players:%s,masterId:%s",$standardTeamId,$standardPlayers,$masterTeamId));
        $standardPlayersArray=json_decode($standardPlayers,true);
        if(empty($standardPlayersArray) || count($standardPlayersArray)==0){
            $this->log(sprintf("no player:%s",json_encode($standardPlayersArray)));
            return;
        }
        $sql=sprintf($sqlTpl,implode(",",$standardPlayersArray));
        $playerInfo=\Yii::$app->getDb()->createCommand($sql)->queryAll();
        $needRefresh=false;
        foreach ($playerInfo as $key=>$val){
            if(empty($val['team_id'])){
                $needRefresh=true;
                break;
            }
        }
        if($needRefresh){
            $this->log(sprintf("start run with need_refresh:%s,players:%s",$needRefresh,json_encode($playerInfo)));
        }
        return $needRefresh;
    }
}