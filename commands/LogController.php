<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use app\modules\task\models\TaskInfo;
use yii\console\Controller;
use yii\console\ExitCode;
use app\modules\task\services\logformat\LogGenerator;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;


/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author WangJinLong
 * @version 1.0
 */
class LogController extends Controller
{

    /**
     * @param int $offset
     * @param int $limit
     * @param $speed
     * @param string $ip
     * @param int $id
     * @return int
     */
    public function actionEvent($offset = 0, $limit = 1000, $speed, $ip = 'test092710', $id = 0,$end_id=0,$incr_id=0)
    {
        $start_time = time();
        $where[]    = ['ip_address' => $ip];

        $ord = ReceiveData5eplayFormatEvents::find();
        $ord->andWhere(['ip_address' => $ip]);
        if ($id > 0) {
            $ord->andWhere(['>=', 'id', $id]);
        }
        if ($end_id > 0) {
            $ord->andWhere(['<=', 'id', $end_id]);
        }
        if ($incr_id > 0) {
            $ord->andWhere(['=', 'id', $incr_id]);
        }
        $list         = $ord->asArray()->offset($offset)->limit($limit)->all();
//        print_r($list);exit;
        $j            = 0;
        $offset_speed = 0;
        $count        = count($list);
        for ($i = 0; $i <= 99999999; $i++) {
            $filter_list = array_slice($list, $offset_speed, $speed);
            if ($offset_speed >= $count) {
                $end_time   = time();
                $spend_time = date('i:s', $end_time - $start_time);
                exit('complete! spend time ' . $spend_time);
            }
            $offset_speed = $i * $speed + $speed;
//            print_r($filter_list);exit;
            foreach ($filter_list as $k => $v) {
                $v['event_id'] = $v['id'];
                $cc['params']  = json_encode($v);
                $res           = FiveHotCsgoMatchWs::run('', $cc);
                $j++;
                $info = json_decode($v['info'], true);
                print_r($j . '-------' . $info['log_time'] . '----------' . $v['id'] . '---------' . $v['event_type'] . PHP_EOL);
//                print_r( $v );
            }
            sleep(1);
        }
        return ExitCode::OK;
    }

    public function actionTest(){
        $start_time = time();
        $id = 226028;
        $id = 20288;
        $id = 191481;
        $id = 327019;//tiger
//        $id = 277892;//dev

        $taskInfo = TaskInfo::find()->where(['id'=>$id])->asArray()->one();

        $res=\app\modules\task\services\hot\five\FiveHotCsgoMatchWsToApi::run('',$taskInfo);
        $end_time   = time();
        $spend_time = date('i:s', $end_time - $start_time);
        print_r($spend_time);exit;
//        $this->assertEquals(
//            1,
//            1
//        );
    }

    /**
     * 通过时间段和ip回放日志
     * 注意:命令行中的字符串参数有空格时需要加双引号
     * @param string $type
     * @param string $originStartTime
     * @param string $originEndTime
     * @param string $areaAddress
     * @param string $virtualIpAddress
     * @param int $speed
     * @param string $url http://admin.gc.comingtrue.cn/v1/receive/receive/debug 开发 http://39.102.46.132/v1/receive/receive/debug  线上
     * @return int
     * @example php yii log/add-info funspark "2020-08-19 13:37:42" "2020-08-19 13:37:55" "127.0.0.2" "127.0.0.3" 10
     */
    public function actionAddInfo($type = 'funspark', $originStartTime = '2020-08-19 02:11:31',
                                  $originEndTime = '2020-08-19 02:13:3', $areaAddress = '127.0.0.7',
                                  $virtualIpAddress='test082602', $speed=10,$url="http://admin.gc.comingtrue.cn/v1/receive/receive/debug")
    {
        $lg=new LogGenerator();
        $lg->addInfo($type,$originStartTime,$originEndTime,$areaAddress,$virtualIpAddress,$speed,$url);
        return ExitCode::OK;//退出代码
    }

    /**
     * @param string $areaAddress 虚拟地址(不能和其他ip重复)
     * @param string $file 读取的文件地址
     * @param string $url 推送到的ip地址
     * @return int
     */
    public function actionAddFileInfo($areaAddress = '127.0.0.1', $file = '', $url = "http://admin.gc.comingtrue.cn/v1/receive/receive/debug",$speed=1,$total=100)
    {
        $lg = new LogGenerator();
        $lg->addFileInfo($areaAddress, $file, $url,$speed,$total);
        return ExitCode::OK;//退出代码
    }

    /**
     * @param string $areaAddress 虚拟地址(不能和其他ip重复)
     * @param string $file 读取的文件地址
     * @param string $url 推送到的ip地址
     * @param int $log_type 1 5e 2 funspark
     * @param int $speed
     * @param int $total
     * @return int
     */
    public function actionAddFileInfoFast($areaAddress = '127.0.0.1', $file = '', $url = "http://admin.gc.comingtrue.cn/v1/receive/receive/debug", $log_type=1)
    {
        $lg = new LogGenerator();
        $lg->addFileInfoFast($areaAddress, $file, $url,$log_type);
        return ExitCode::OK;//退出代码
    }

    /**
     * @param string $areaAddress 虚拟地址(不能和其他ip重复)
     * @param string $file 读取的文件地址
     * @param string $url 推送到的ip地址
     * @return int
     */
    public function actionLogGeneratorTest($areaAddress = '127.0.0.1', $file = '', $url = "http://admin.gc.comingtrue.cn/v1/receive/receive/debug")
    {
        $lg = new LogGenerator();
        $lg->addInfo($areaAddress, $file, $url);
        return ExitCode::OK;//退出代码
    }
    /**
     * @param string $areaAddress 虚拟地址(不能和其他ip重复)
     * @param string $file 读取的文件地址
     * @param string $url 推送到的ip地址
     * @return int
     */
    public function actionWorkerman()
    {
//        echo '13';exit;
//        $http_worker = new Worker("websocket://0.0.0.0:2345");
//        // 启动4个进程对外提供服务
//        $http_worker->count = 4;
//
//        // 接收到浏览器发送的数据时回复hello world给浏览器
//        $http_worker->onMessage = function($connection, $data)
//        {
//            // 向浏览器发送hello world
//            $connection->send('hello world');
//        };
//
//        // 运行worker
//       $res = Worker::runAll();
//       print_r($res);
//        return ExitCode::OK;//退出代码
    }
}