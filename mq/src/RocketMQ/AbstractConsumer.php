<?php
namespace WBY\MQ\SDK\RocketMQ;

use App\Library\Context\Fact\FactException;
use app\modules\common\models\CommonLog;
use app\modules\common\services\CommonLogService;
use MQ\Exception\AckMessageException;
use MQ\Exception\MessageNotExistException;
use MQ\Model\Message;
use MQ\MQConsumer;
use WBY\MQ\SDK\AbstractSubscriber;
use yii\log\Logger;

/**
 * Class AbstractConsumer
 * @package WBY\MQ\SDK\RocketMQ
 */
abstract class AbstractConsumer extends AbstractSubscriber
{
    protected $consumer;

    protected $numOfMessages, $waitSeconds, $sleepSeconds;

    /**
     * AbstractConsumer constructor.
     * @param MQConsumer $consumer
     * @param int $numOfMessages 一次最多消费3条(最多可设置为16条)
     * @param int $waitSeconds 长轮询时间3秒（最多可设置为30秒）
     */
    public function __construct(MQConsumer $consumer, $numOfMessages = 3, $waitSeconds = 3, $sleepSeconds=2)
    {
        $this->consumer = $consumer;
        $this->numOfMessages = $numOfMessages;
        $this->waitSeconds = $waitSeconds;
        $this->sleepSeconds = $sleepSeconds;
    }

    /**
     * 单次处理逻辑
     * 批量成功 批量失败
     * @throws \Exception
     * @throws \MQ\AckMessageException
     * @throws \MQ\MessageNotExistException
     * @throws \MQ\ReceiptHandleErrorException
     */
    public function consume()
    {
        $messages = $this->consumeMessage();
        if (!empty($messages)) {
            $receiptHandles = [];
            $exceptions = [];
            foreach ($messages as $message) {
                try{
                    $this->businessProcess($message);
                    $receiptHandles[] = $message->getReceiptHandle();
                }catch (\Exception $e){
                    CommonLogService::recordException($e);
                    $exceptions[] = $message;
                }
            }
            if(count($receiptHandles) > 0){
                $this->ackMessage($receiptHandles);
            }

            if(count($exceptions) > 0 ){
                $total = count($messages);
                $success = count($receiptHandles);
                $fail = count($exceptions);
                throw new \Exception("未全部成功消费（全部:{$total},成功：{$success}，失败：{$fail}）！");
            }
            return $messages;
        }
    }

    /**
     * 消费队列中消息
     * @return Message
     */
    function consumeMessage()
    {
        return $this->consumer->consumeMessage($this->numOfMessages, $this->waitSeconds);
    }

    /**
     * 消费成功后回执消息已消费
     * @param array $receiptHandles
     * @return \MQ\Responses\AckMessageResponse
     * @throws \MQ\AckMessageException
     * @throws \MQ\ReceiptHandleErrorException
     */
    function ackMessage(array $receiptHandles)
    {
        return $this->consumer->ackMessage($receiptHandles);
    }

    /**
     * 守护进程入口函数
     * @throws \Exception
     * @throws \MQ\AckMessageException
     * @throws \MQ\MessageNotExistException
     * @throws \MQ\ReceiptHandleErrorException
     */
    public function daemon()
    {
        while (true) {
            try {
                $this->consume();
            } catch (\Exception $e) {
                if ($e instanceof MessageNotExistException) {
                    sleep($this->sleepSeconds);
                    continue;
                } elseif ($e instanceof AckMessageException) {
                    $this->handleAckException($e);
                } else {
                    $this->handleExcetpion($e);
                }
            }
        }
    }
}