<?php
namespace WBY\MQ\SDK\Interfaces;

interface DeamonInterface
{
    /**
     * 守护进程入口函数
     * @return mixed
     */
    function daemon();
}