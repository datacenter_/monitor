<?php
namespace WBY\MQ\SDK;

use WBY\MQ\SDK\RocketMQ\Client as RocketMQClient;
use WBY\MQ\SDK\ProxyMQ\Client as ProxyMQClient;

/**
 * Class MQClientFactory
 * 简单工厂 获取MQ Client
 * @package WBY\MQ\SDK
 */
class MQClientFactory
{
    const ROCKETMQ = "RocketMQ";
    const PROXYMQ = "ProxyMQ";
    private static $mqClients;

    /**
     * 获取
     * @param string $identity
     * @return object
     */
    private static function getClient($identity)
    {
        $instanceIdentity = $identity;
        if(self::$mqClients && isset(self::$mqClients[$instanceIdentity])){
            return self::$mqClients[$instanceIdentity];
        }
    }

    /**
     * 保存
     * @param String $identity
     * @param object $instance
     */
    private static function saveClient($identity, $instance)
    {
        $instanceIdentity = $identity;

        self::$mqClients[$instanceIdentity] = $instance;
    }

    /**
     * @param array $config
     * ```
     * $config = [
     *      'endPoint' => '',       //required
     *      'accessId' => '',       //required
     *      'accessKey' => '',      //required
     *      'securityToken' => '',  // default null
     *      'proxy' => '',          // default null
     *      'requestTimeout' => '', // default null
     *      'connectTimeout' => '', // default null
     *      'expectContinue' => '', // default null
     * ]
     * ```
     * @return RocketMQClient
     */
    public static function getRocketMQClient(array $config)
    {
        $client = self::getClient(self::ROCKETMQ);
        if (empty($client)) {
            $client = new RocketMQClient($config);
            self::saveClient(self::ROCKETMQ, $client);
        }

        return self::getClient(self::ROCKETMQ);
    }

    /**
     * @deprecated
     * @param $endPoint string
     * @return ProxyMQClient
     */
    public static function getProxyMQClient($endPoint)
    {
        $key = self::PROXYMQ . ":" . md5($endPoint);
        $client = self::getClient($key);
        if (empty($client)) {
            $client = new ProxyMQClient($endPoint);
            self::saveClient($key, $client);
        }

        return self::getClient($key);
    }

}