<?php
$onAfterOpen = function ($event) {
    $event->sender->createCommand("SET @@session.sql_mode=\"STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION\"")
        ->execute();
};

return [
    'class' => \yii\db\Connection::class,
    'on afterOpen' => $onAfterOpen,
    'dsn' => sprintf("mysql:host=%s;port=%s;dbname=%s", env('MASTER_DB_HOST'), env('MASTER_DB_PORT'), env('MASTER_DB_DATABASE')),
    'username' => env('MASTER_DB_USERNAME'),
    'password' => env('MASTER_DB_PASSWORD'),
    'charset' => env('MASTER_DB_CHARSET'),
    'slaveConfig' => [
        'class' => \yii\db\Connection::class,
        'on afterOpen' => $onAfterOpen,
        'username' => env('SLAVE_DB_USERNAME'),
        'password' => env('SLAVE_DB_PASSWORD'),
        'charset' => env('SLAVE_DB_CHARSET'),
        'attributes' => [
            PDO::ATTR_TIMEOUT => 10,
        ],
    ],
    'slaves' => [
        ['dsn' => sprintf("mysql:host=%s;port=%s;dbname=%s", env('SLAVE_DB_HOST'), env('SLAVE_DB_PORT'), env('SLAVE_DB_DATABASE'))],
    ],
];
