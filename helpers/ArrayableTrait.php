<?php
namespace yii\base;

trait ArrayableTrait
{
    public function fields()
    {
        $fields = array_keys(\Yii::getObjectVars($this));
        return array_combine($fields, $fields);
    }

    public function extraFields()
    {
        return [];
    }

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $data = [];
        foreach ($this->resolveFields($fields, $expand) as $field => $definition) {
            $data[$field] = is_string($definition) ? $this->$definition : call_user_func($definition, $this, $field);
        }

        $expands = $this->expand($expand, $data);

        return $recursive ? ArrayHelper::toArray($data, [], true, $expands) : $data;
    }

    protected function expand(array $expand, $data = [])   // <-- add this
    {
        $expands = [];
        foreach ($expand as $field) {
            if (in_array($field, $this->fields())) {
                continue;
            }

            $fields = explode('.', $field, 2);
            if (count($fields) == 1 && isset($data[$field])) {
                if (is_object($data[$field])) {
                    $fieldData = $data[$field];
                } elseif (is_array($data[$field]) && isset($data[$field][0]) && is_object($data[$field][0])) {
                    $fieldData = $data[$field][0];
                }

                if (isset($fieldData)) {
                    $expands[$fields[0]] = array_merge($expands[$fields[0]] ?? [], array_keys($fieldData->fields()));
                }
            } else {
                $expands[$fields[0]][] = $fields[1] ?? false;
            }
        }

        foreach ($expands as $key => $value) {
            $expands[$key] = array_filter($value);
        }

        return $expands;
    }

    protected function resolveFields(array $fields, array $expand)
    {
        $result = [];

        foreach ($this->fields() as $field => $definition) {
            if (is_int($field)) {
                $field = $definition;
            }
            if (empty($fields) || in_array($field, $fields, true)) {
                if (($key = array_search($field, $fields)) && is_string($key)) {
                    $result[$key] = $definition;
                } else {
                    $result[$field] = $definition;
                }
            }
        }

        if (empty($expand)) {
            return $result;
        }

        $expands = $this->expand($expand);

        foreach ($this->extraFields() as $field => $definition) {
            if (is_int($field)) {
                $field = $definition;
            }
            if (isset($expands[$field])) {
                $result[$field] = $definition;
            }
        }

        return $result;
    }
}
