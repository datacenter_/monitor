<?php
namespace app\components;

use app\rest\Code;
use app\rest\exceptions\BusinessException;
use app\rest\exceptions\RestException;
use app\rest\StatusCode;
use yii\base\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * 响应内容
 * Class Response
 * @package app\components
 */
class Response extends \yii\web\Response
{
    /**
     * @var string json only
     */
    public $format = self::FORMAT_JSON;

    /**
     * @var array
     */
    public $targetFormat;

    /**
     * @var int 重新定义Code码
     */
    protected $code;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var array 异常情况下的提示信息，response.data的内容
     */
    protected $suggests;

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_BEFORE_SEND, [$this, 'beforeSend']);
    }

    /**
     * ```response content
     * [
     *      code => number,
     *      message => "",
     *      date => []
     * ]
     * ```
     */
    public function beforeSend()
    {
//        if (\Yii::$app->getRequest()->isOptions) {
//            $this->format = self::FORMAT_RAW;
//            $this->data = null;
//            return;
//        }

        if (!in_array($this->format, $this->targetFormat)) {
            return;
        }

        if ($this->isSuccessful) {
            $code = Code::SUCCESS_CODE;
            $this->data = [
                'code' => intval($code),
                'message' => \Yii::t('app', "success"),
                'data' => $this->data
            ];
        } else {
            $message = $this->getMessage();
            if (empty($message)) {
                $message = Code::getExceptionMessageByCode($this->getCode());
            }
            $this->data = [
                'code' => $this->getCode(),
                'message' => $message,
                'data' => YII_DEBUG ? ArrayHelper::merge(['___debug' => $this->data], $this->getSuggests()) : $this->getSuggests()
            ];
        }

        $this->statusCode = Code::HTTP_SUCCESS_CODE;
    }

    /**
     * @return bool whether this response is successful
     */
    public function getIsSuccessful()
    {
        return $this->getCode() == Code::SUCCESS_CODE;
    }

    /**
     * @inheritdoc
     * @param \Error|\Exception $e
     * @return $this
     */
    public function setStatusCodeByException($e)
    {
        if ($e instanceof RestException) {
            $this->setCode($e->getCode());
            $this->setSuggests($e->getData());
            $this->setMessage($e->getMessage());
            \Yii::error('发生异常->' . $e->getMessage());
            \Yii::error('异常堆栈信息->' . $e->getTraceAsString());
        } else {
            $this->setCode(Code::BUSINESS_EXCEPTION);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code ?: Code::SUCCESS_CODE;
    }

    /**
     * @param int $code
     */
    public function setCode($code): Response
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message ?: "";
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): Response
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return array
     */
    public function getSuggests(): array
    {
        return $this->suggests ?: [];
    }

    /**
     * @param array $suggests
     */
    public function setSuggests(array $suggests): void
    {
        $this->suggests = $suggests;
    }

}
