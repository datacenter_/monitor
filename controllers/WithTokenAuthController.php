<?php
namespace app\controllers;

use app\components\IdentityActiveRecord;
use app\filters\TokenAuthMethod;

/**
 * Token认证(登录状态)控制器
 * Class WithAuthFilterController
 * @package app\controllers
 */
class WithTokenAuthController extends RestController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => TokenAuthMethod::class,
            'user' => $this->module->get('user'),
        ];
        return $behaviors;
    }

    /**
     * @return IdentityActiveRecord
     */
    public function getIdentity()
    {
        return $this->module->get('user')->getIdentity();
    }

    /**
     * @return string
     */
    public function getIentityToken()
    {
        return \Yii::$app->request->getHeaders()->get(TokenAuthMethod::$tokenKey);
    }
}