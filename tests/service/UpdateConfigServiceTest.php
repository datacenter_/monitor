<?php
/**
 *
 */

use app\modules\data\models\DataUpdateConfigDefault;
use app\modules\data\services\UpdateConfigService;

class UpdateConfigServiceTest extends PHPUnit\Framework\TestCase
{
    public function testAu()
    {
        UpdateConfigService::initDefaultConfig();
    }

    public function testDiff()
    {
        $info='{"old":[],"new":{"game_id":"6","resource_type":"team","tag_type":"team_player","origin_id":1,"update_type":1,"id":51},"diff":[]}';
        $array=json_decode($info,true);
        $diff=\app\modules\task\services\Common::getDiffInfo($array["old"],$array["new"],['origin_id','update_type']);
        print_r($diff);
        $this->assertTrue(true);
    }
    public function testList()
    {
        $list=UpdateConfigService::getDefaultConfigList();
        print_r($list);
    }

    public function testDetail()
    {
        $list=UpdateConfigService::getDefaultConfigDetail(1,'team');
        print_r($list);
    }

    public function testUpdate()
    {
        $list=UpdateConfigService::setDefaultInfo(1,['team.core_data'=>['origin_id'=>1,'update_type'=>2]]);
        print_r($list);
    }

    public function testOperationLog()
    {
        $info=\app\modules\common\services\OperationLogService::getOperationLog(['resource_type'=>'sys_update_config_default','resource_id'=>'1']);
        print_r($info);
    }

    public function testOpenArray()
    {
        $ar=[
            'a'=>["b"=>1]
        ];
        print_r(\app\modules\task\services\Common::openArray($ar));
    }

    public function testInitConfig()
    {
        // 初始化一个resource的配置
        $info=UpdateConfigService::initResourceUpdateConfig(22,\app\modules\common\services\Consts::RESOURCE_TYPE_PLAYER,1);
        print_r($info);

        $info='{
  "game_id": "1",
  "config": {
    "team.core_data": {
      "origin_id": 1,
      "update_type": 1
    },
    "team.base_data": {
      "origin_id": 1,
      "update_type": 1
    },
    "team.team_player": {
      "origin_id": 1,
      "update_type": 1
    }
  }
}';

    }
}