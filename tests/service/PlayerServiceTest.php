<?php
/**
 *
 */

class PlayerServiceTest extends PHPUnit\Framework\TestCase
{
    public function testDetail()
    {
        $detail=\app\modules\org\services\PlayerService::getPlayer(24);
        print_r($detail);
        exit;
    }

    public function testAdd()
    {
        $info='{"core_data":{"player_name":"5555","game_id":1,"player_country":"2","real_name":"姓名英文","real_name_cn":"姓名中文","logo":"http://shaoxiatec.oss-cn-beijing.aliyuncs.com/type_logo/82947919e8de19b32a1311e0b9686876/u%3D943737565%2C604373982%26fm%3D11%26gp%3D0.jpg"},"base_data":{"steam_id":"","age":"","introduction_cn":"","introduction":""}}';
        $info=json_decode($info,true);
        $re=\app\modules\org\services\PlayerService::setPlayer($info,1,1);
        print_r($re);
    }

    public function testUpdate()
    {
//        $data='{"player_name":"昵称8","game_id":"3","logo":"http://shaoxiatec.oss-cn-beijing.aliyuncs.com/type_logo/85b6c4b370ed846326dbebe9b5cf5208/u%3D1150977256%2C3128490420%26fm%3D111%26gp%3D0.jpg","real_name":"姓名英文","real_name_cn":"姓名中文","steam_id":"ID","age":"年龄","introduction":"简介英文","introduction_cn":"简介中文"}';
        $data='{
        "player_id":"24",
    "core_data": {
      "player_name": "选手昵称",
      "game_id": "3",
      "player_country": "4",
      "real_name": "姓名英文",
      "real_name_cn": "姓名中文",
      "logo": "http://shaoxiatec.oss-cn-beijing.aliyuncs.com/type_logo/635251dfab79a18c2295fde19477b9e2/3393d8b29f913d4f09132057945bef9b.jpeg",
      "deleted": "0",
      "cuser": "22",
      "created_at": "2020-03-18 14:43:31",
      "modified_at": "2020-03-18 14:43:31"
    },
    "base_data": {
      "steam_id": "1",
      "age": "1",
      "introduction": "",
      "introduction_cn": ""
    },
    "team_player_relation": [
      {
        "id": "6",
        "team_id": "11",
        "player_id": "24",
        "status": "0",
        "name": "野狼全称"
      },
      {
        "id": "34",
        "team_id": "13",
        "player_id": "24",
        "status": "0",
        "name": "全称"
      }
    ]
  }';
        $info=json_decode($data,true);
//        print_r($info);
        $re=\app\modules\org\services\PlayerService::setPlayer($info,1,1);
        print_r($re);
    }
}