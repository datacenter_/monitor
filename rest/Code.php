<?php
namespace app\rest;

/**
 * 状态码
 * Class Code
 * @package app\rest
 */
class Code extends \yii\rest\Serializer
{
    /**
     * http成功状态码，默认返回http状态码都是此码
     */
    const HTTP_SUCCESS_CODE = 200;

    /**
     * 成功
     */
    const SUCCESS_CODE = 200;

    /**
     * 业务运行时异常，一般是数据处理的问题、逻辑不严谨等，比如:已经验证通过 存数据库时依然失败
     */
    const RUNTIME_EXCEPTION = 10500;

    /**
     * 未授权
     */
    const UNAUTHORIZATION_EXCEPTION = 10401;

    /**
     * 认证失败
     */
    const AUTHENTICATION_FAILED_EXCEPTION = 10403;

    /**
     * 未登录
     */
    const NO_LOGGED_IN_EXCEPTION = 10400;

    /**
     * 业务失败
     */
    const BUSINESS_EXCEPTION = 10466;

    /**
     * 数据不存在
     */
    const ENTITY_NOT_EXIST_EXCEPTION = 10610;

    /**
     * 参数不合法
     */
    const REQUEST_PARAMS_UNVALIDATED_EXCEPTION = 10601;

    /**
     * 没有操作权限
     */
    const NO_OPERATION_PERMISSION_EXCEPTION = 10602;

    /**
     * 远程调用异常
     */
    const REMOTE_CALL_EXCEPTION = 10699;

    /**
     * 默人异常信息
     */
    const DEFAULT_EXCEPTION_MESSAGE = '系统异常,请稍后操作!';

    /**
     * 异常默认信息
     * @var array
     */
    public static $exceptionDefaultMessages = [
        self::BUSINESS_EXCEPTION => 'business exception',
        self::UNAUTHORIZATION_EXCEPTION => 'unauthorization exception',
        self::AUTHENTICATION_FAILED_EXCEPTION => 'authentication failed exception',
        self::NO_LOGGED_IN_EXCEPTION => 'unsignin exception',
        self::REQUEST_PARAMS_UNVALIDATED_EXCEPTION => 'request params unvalidated exception',
        self::NO_OPERATION_PERMISSION_EXCEPTION => 'no operation permission',
        self::RUNTIME_EXCEPTION => 'runtime exception',
        self::ENTITY_NOT_EXIST_EXCEPTION => 'entity not exist exception',
        self::REMOTE_CALL_EXCEPTION => 'remote call exception'
    ];

    /**
     * 根据错误码返回异常信息
     * @param int $code
     * @return string
     */
    public static function getExceptionMessageByCode($code)
    {
        return \Yii::t('app.exception', self::$exceptionDefaultMessages[$code]) ?? \Yii::t('app.exception', 'default exception message');
    }

}