<?php
namespace app\rest\exceptions;

use app\rest\Code;
use yii\web\HttpException;

/**
 * 异常基类
 * Class RestException
 * @package app\rest\exceptions
 * @property array $data
 */
class RestException extends HttpException
{
    /**
     * @var array
     */
    private $data;

    /**
     * RestException constructor.
     * @param array $data 提示信息或需要返回的数据
     * @param string $message 错误信息
     * @param \Exception|null $previous
     */
    public function __construct($data = [], $message = null, \Exception $previous = null)
    {
        $this->data = $data;

        parent::__construct(null, $message, $this->getErrCode(), $previous);
    }

    /**
     * @inheritdoc
     * @param string $message
     * @return string
     */
    public function getName()
    {
        return __CLASS__;
    }

    /**
     * 唯一状态码
     * @return int
     */
    public function getErrCode(): int
    {
        return Code::SUCCESS_CODE;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}


