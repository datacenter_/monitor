<?php
namespace app\rest\exceptions;

use app\rest\Code;

/**
 * 调用第三方接口异常
 * Class RemoteServerException
 * @package app\rest\exceptions
 */
class RemoteCallException extends BusinessException
{
    public function getErrCode(): int
    {
        return Code::REMOTE_CALL_EXCEPTION;
    }
}