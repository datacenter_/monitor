<?php
namespace app\rest\exceptions;

use app\rest\Code;

/**
 * 业务异常
 * Class BusinessException
 * @package app\rest\exceptions
 */
class BusinessException extends RestException
{
    public function getErrCode(): int
    {
        return Code::BUSINESS_EXCEPTION;
    }
}