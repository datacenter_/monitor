<?php
namespace app\rest\exceptions;

use app\rest\Code;

/**
 * 运行时异常,校验成功，但操作失败
 * Class RuntimeException
 * @package app\rest\exceptions
 */
class RuntimeException extends RestException
{
    public function getErrCode(): int
    {
        return Code::RUNTIME_EXCEPTION;
    }
}