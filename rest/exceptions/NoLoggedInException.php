<?php
namespace app\rest\exceptions;

use app\rest\Code;

/**
 * 未登录
 * Class NoLoggedInException
 * @package app\rest\exceptions
 */
class NoLoggedInException extends RestException
{
    public function getErrCode(): int
    {
        return Code::NO_LOGGED_IN_EXCEPTION;
    }
}