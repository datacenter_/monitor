<?php
return [
    'components' => [
        'urlManager' => [
            'rules' => [
                [
                    'class' => 'yii\web\GroupUrlRule',
                    'prefix' => 'v1',
                    'rules' => [
                        'GET <module>/<controller>' => '<module>/<controller>/index',
                        '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
                    ]
                ]
            ]
        ],
    ],
];