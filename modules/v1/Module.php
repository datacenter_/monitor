<?php

namespace app\modules\v1;

/**
 * v1 module definition class
 */
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();

        $this->modules = [
            'warning' => [
                'class' => \app\modules\warning\Module::class,
            ],
        ];
    }
}
