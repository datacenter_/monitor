<?php

namespace app\modules\warning\models;

use Yii;

/**
 * This is the model class for table "warning_info".
 *
 * @property int $id
 * @property string|null $type 类别
 * @property string|null $tag
 * @property string|null $info 错误信息
 * @property string|null $created_at
 * @property string|null $modified_at
 */
class WarningInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'warning_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['info'], 'string'],
            [['created_at', 'modified_at'], 'safe'],
            [['type'], 'string', 'max' => 20],
            [['tag'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'tag' => 'Tag',
            'info' => 'Info',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }
}
