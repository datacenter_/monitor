<?php
namespace app\modules\warning\services;

use app\modules\common\services\DingGroup;
use app\modules\warning\models\WarningInfo;
use app\rest\exceptions\BusinessException;

class WarningService
{
    public static function setMessage($params) {
        if (empty($params)) {
            return null;
        }

        $warningInfo = new WarningInfo();
        $data = [
            'type' => $params['type'],
            'tag' => $params['tag'],
            'info' => $params['info'],
        ];

        $warningInfo -> setAttributes($data);
        if (!$warningInfo->save()) {
            throw new BusinessException($warningInfo->getErrors(), '保存任务失败');
        }
        if ($data['type'] == 'emptyPlatformName') {
            $liveurl = explode("liveUrl:",$params['info']);
            $LiveUrlNotification = DingGroup::ontInform($liveurl[1]);
            if (!$LiveUrlNotification) {
                return false;
            }
        }

        if ($data['type'] == 'task_info') {
            $taskInfo = json_decode($params['info'],true);
            $taskInfoTag = explode("\n",$params['tag']);


            if (strpos($taskInfo['error_info'],'SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry') !== false) {
                $NotificationTag = DingGroup::ontInTag($taskInfoTag[1]);
                if (!$NotificationTag) {
                    return false;
                }

            }
        }




        $key = self::buildWarningKey($warningInfo['type'],$warningInfo['tag']);
        $exKey = \Yii::$app->getRedis()->exists($key);

        if (!$exKey) {
           $success =  self::sendMessage($key,$warningInfo['info']);

        }

        \Yii::$app->getRedis()->multi();
        if (!$exKey) {
            \Yii::$app->getRedis()->set($key,$warningInfo['info']);
            \Yii::$app->getRedis()->expire($key, 300);
        }
       \Yii::$app->getRedis()->exec();

        if ($data['type'] ='daLei') {
            $lastTime = date("Y-m-d H:i:s", strtotime("-5 minute"));
            $daLeiCount = WarningInfo::find()->where(
                ['and',
                    ['=','type','daLei'],
                    ['=','tag',$data['tag']],
                    ['>=','created_at',$lastTime]
                ]
            )->asArray()->count();
            if ($daLeiCount >= '5' && !$exKey){
                $success = self::sendMessage($key,$warningInfo['info']);
            }
        }
        return $success;

    }


    public static function buildWarningKey($type, $tag)
    {

        return sprintf("%s---%s", $type, $tag);
    }



    public static function sendMessage($key,$info) {
        $webhook = DingGroup::computeSignature(DingGroup::TASK_INFO_ERROR_MESSAGE['secret'],DingGroup::TASK_INFO_ERROR_MESSAGE['webhook']);
        $tag = explode('---',$key);

        if ($tag[0] == 'task_info' ) {
            $messageArr = json_decode($info,true);

            $error_info = json_decode($messageArr['error_info'],true);
            $message="业务报警 : id:{$messageArr['id']} , run_type类别:{$messageArr['run_type']} , batch_id:{$messageArr['batch_id']}\r," ."tag :  {$tag[1]}"."\r".
                "error_info : {$error_info['msg']}   \r {$error_info['file']}  \r,{$error_info['line']} 行。" ."\r".
                " debug_info: ".$messageArr['debug_info'];
            $data = array ('msgtype' => 'text','text' => array ('content' => $message),
                'at' =>DingGroup::TASK_INFO_ERROR_MESSAGE['atMobiles']
            );

            $data_string = json_encode($data,JSON_UNESCAPED_UNICODE);
            $result = DingGroup::requestByCurl($webhook, $data_string);
            return "monitor:".$result;

        }

        if ($tag[0] == "task_info_reuse") {
            $reusemessage ="业务报警tag :  {$tag[1]} ,error_info:{$info}";
            $reusedata = array ('msgtype' => 'text','text' => array ('content' => $reusemessage),
                'at' =>[]
            );
            $reuse_data_string = json_encode($reusedata,JSON_UNESCAPED_UNICODE);
            $reuseresult = DingGroup::requestByCurl($webhook, $reuse_data_string);
            return "monitor:".$reuseresult;;

        }


        if ($tag[0] == 'daLei' ) {
            $daLeimessage ="业务报警tag :  {$tag[1]} ,error_info:{$info}";
            $daLeidata = array ('msgtype' => 'text','text' => array ('content' => $daLeimessage),
                'at' =>DingGroup::DA_LEI['atMobiles']
            );
            $daLei_data_string = json_encode($daLeidata,JSON_UNESCAPED_UNICODE);
            $daLeiresult = DingGroup::requestByCurl($webhook, $daLei_data_string);
            return "monitor:".$daLeiresult;

        }

        if ($tag[0] == 'EsMatch' && !empty($info)) {
            $EsMatchmessage ="业务报警tag :  {$tag[1]} ,error_info:{$info}";
            $EsMatchdata = array ('msgtype' => 'text','text' => array ('content' => $EsMatchmessage),
                'at' =>DingGroup::ES_MATCH['atMobiles']
            );
            $EsMatchdatadata_string = json_encode($EsMatchdata,JSON_UNESCAPED_UNICODE);
            $EsMatchresult = DingGroup::requestByCurl($webhook, $EsMatchdatadata_string);
            return "monitor:".$EsMatchresult;
        }


        if ($tag[0] == 'EsErrorMatchHK') {
            $EsErrorMatchHKmessage ="业务报警tag :  {$tag[1]} ,error_info:{$info}";
            $EsErrorMatchHKdata = array ('msgtype' => 'text','text' => array ('content' => $EsErrorMatchHKmessage),
                'at' =>DingGroup::ES_MATCH['atMobiles']
            );
            $EsErrorMatchHKdata_string = json_encode($EsErrorMatchHKdata,JSON_UNESCAPED_UNICODE);
            $EsErrorMatchHKsult = DingGroup::requestByCurl($webhook, $EsErrorMatchHKdata_string);
            return "monitor:".$EsErrorMatchHKsult;
        }

        if ($tag[0] == 'EsErrorMatchSgp') {
            $EsErrorMatchSgpmessage = "业务报警tag :  {$tag[1]} ,error_info:{$info}";
            $EsErrorMatchSgpdata = array('msgtype' => 'text', 'text' => array('content' => $EsErrorMatchSgpmessage),
                'at' => DingGroup::ES_MATCH['atMobiles']
            );
            $EsErrorMatchSgpdata_string = json_encode($EsErrorMatchSgpdata, JSON_UNESCAPED_UNICODE);
            $EsErrorMatchSgpsult = DingGroup::requestByCurl($webhook, $EsErrorMatchSgpdata_string);
            return "monitor:" . $EsErrorMatchSgpsult;
        }


        if ($tag[0] == 'emptyPlatformName') {
            $emptyPlatformNamemessage ="业务报警tag :  {$tag[1]} ,error_info:{$info}";
            $emptyPlatformNamedata = array ('msgtype' => 'text','text' => array ('content' => $emptyPlatformNamemessage),
                'at' =>DingGroup::EMPTY_PLATFORM_NAME['atMobiles']
            );
            $emptyPlatformNamedata_string = json_encode($emptyPlatformNamedata,JSON_UNESCAPED_UNICODE);
            $emptyPlatformNameresult = DingGroup::requestByCurl($webhook, $emptyPlatformNamedata_string);
            return "monitor:".$emptyPlatformNameresult;
        }

        if ($tag[0] == 'matchBack_shuai'){
            $matchBack_shuaimessage ="业务报警tag :  {$tag[1]} ,error_info:{$info}";
            $matchBack_shuaidata = array ('msgtype' => 'text','text' => array ('content' => $matchBack_shuaimessage),
                'at' =>DingGroup::EMPTY_PLATFORM_NAME['atMobiles']
            );
            $matchBack_shuaidata_string = json_encode($matchBack_shuaidata,JSON_UNESCAPED_UNICODE);
            $matchBack_shuairesult = DingGroup::requestByCurl($webhook, $matchBack_shuaidata_string);
            return "monitor:".$matchBack_shuairesult;
        }

        if ($tag[0] == 'ErrorNotice') {

            $ErrorNoticemessage ="业务报警tag :  {$tag[1]} ,error_info:{$info}";
            $ErrorNoticedata = array ('msgtype' => 'text','text' => array ('content' => $ErrorNoticemessage),
                'at' =>[]
            );
            $ErrorNoticedata_string = json_encode($ErrorNoticedata,JSON_UNESCAPED_UNICODE);
            $ErrorNoticeresult = DingGroup::requestByCurl($webhook, $ErrorNoticedata_string);
            return "monitor:".$ErrorNoticeresult;
        }

        if ($tag[0] == 'IsBattlePush') {

            $MatchPushBattlemessage ="业务报警tag :  {$tag[1]} ,error_info:{$info}";
            $MatchPushBattledata = array ('msgtype' => 'text','text' => array ('content' => $MatchPushBattlemessage),
                'at' =>[]
            );
            $MatchPushBattledata_string = json_encode($MatchPushBattledata,JSON_UNESCAPED_UNICODE);
            $MatchPushBattledatasult = DingGroup::requestByCurl($webhook, $MatchPushBattledata_string);
            return "monitor:".$MatchPushBattledatasult;
        }

        if ($tag[0] == 'CheckEsApi赛事') {

            $CheckEsApiTournamentsMs ="业务报警tag :  {$tag[1]} ,error_info:{$info}";
            $CheckEsApiTournamentsMsdata = array ('msgtype' => 'text','text' => array ('content' => $CheckEsApiTournamentsMs),
                'at' =>[]
            );
            $CheckEsApiTournamentstring = json_encode($CheckEsApiTournamentsMsdata,320);
            $CheckEsApiTournamentsult = DingGroup::requestByCurl($webhook, $CheckEsApiTournamentstring);
            return "monitor:".$CheckEsApiTournamentsult;
        }

        if ($tag[0] == 'CheckEsApi比赛') {

            $CheckEsApiMatchMs ="业务报警tag :  {$tag[1]} ,error_info:{$info}";
            $CheckEsApiMatchdata = array ('msgtype' => 'text','text' => array ('content' => $CheckEsApiMatchMs),
                'at' =>[]
            );
            $CheckEsApiMatchstring = json_encode($CheckEsApiMatchdata,320);
            $CheckEsApiMatchsult = DingGroup::requestByCurl($webhook, $CheckEsApiMatchstring);
            return "monitor:".$CheckEsApiMatchsult;
        }

        if ($tag[0] == 'CheckEsApi选手') {

            $CheckEsApiPlayerMs ="业务报警tag :  {$tag[1]} ,error_info:{$info}";
            $CheckEsApiPlayerdata = array ('msgtype' => 'text','text' => array ('content' => $CheckEsApiPlayerMs),
                'at' =>[]
            );
            $CheckEsApiPlayerstring = json_encode($CheckEsApiPlayerdata,320);
            $CheckEsApiPlayersult = DingGroup::requestByCurl($webhook, $CheckEsApiPlayerstring);
            return "monitor:".$CheckEsApiPlayersult;
        }

        if ($tag[0] == 'CheckEsApi战队') {

            $CheckEsApiTeamMs ="业务报警tag :  {$tag[1]} ,error_info:{$info}";
            $CheckEsApiTeamdata = array ('msgtype' => 'text','text' => array ('content' => $CheckEsApiTeamMs),
                'at' =>[]
            );
            $CheckEsApiTeamstring = json_encode($CheckEsApiTeamdata,320);
            $CheckEsApiTeamsult = DingGroup::requestByCurl($webhook, $CheckEsApiTeamstring);
            return "monitor:".$CheckEsApiTeamsult;
        }

    }
}