<?php
namespace app\modules\warning\controllers;

use app\controllers\RestController;
use app\modules\warning\services\WarningService;

class WarningController extends RestController
{
    public function actionMessage(){
        $params= $this->pPost();
        $success = WarningService::setMessage($params);
        return $success;
    }

}