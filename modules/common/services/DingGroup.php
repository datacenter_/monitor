<?php
namespace app\modules\common\services;

class DingGroup
{
    const TASK_INFO_ERROR_MESSAGE = [
        "secret"=>'SEC617230a7b574f5aef8b8373183abcc08616fe03128b242767adb4e4cee298e8e',
        "webhook"=>'https://oapi.dingtalk.com/robot/send?access_token=ce42b35e2747a0f83b8f6e1fb5d6d1523cb9e1ff3a860ea5f48736389fd14ac9',
        'atMobiles'=>['atMobiles'=>['15710099947','15030001195'],"isAtAll"=>false]
//        "secret"=>'SEC248d3a9a2060a5cb558282f3736ff2ba987d48b254f37f7ea72b129d68f3622a',
//        "webhook"=>'https://oapi.dingtalk.com/robot/send?access_token=3b58c13eb03a8cbd854f336dec3197531d93c8a3a781e26ece7f2335ce4cc5c8',
//        'atMobiles'=>['atMobiles'=>['15710099947','15030001195'],"isAtAll"=>false]
    ];

    const ES_MATCH = [
        'atMobiles'=>['atMobiles'=>['18513334352'],"isAtAll"=>false]
    ];
    const DA_LEI = [
        'atMobiles'=>['atMobiles'=>['18513334352','18334705242'],"isAtAll"=>false]
    ];

    const EMPTY_PLATFORM_NAME = [
        'atMobiles'=>['atMobiles'=>['15710099947'],"isAtAll"=>false]
    ];

    /**
     * 获取签名
     * @param $accessSecret
     * @param $accessKey
     * @return string
     */
    public static function computeSignature ($accessSecret,$accessKey) {

        list($s1, $s2) = explode(' ', microtime());
        $timestamp = (float)sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
        $data = $timestamp . "\n" . $accessSecret;
        $signStr = base64_encode(hash_hmac('sha256', $data, $accessSecret,true));
        $signStr = utf8_encode(urlencode($signStr));
        $accessKey .= "&timestamp=$timestamp&sign=$signStr";
        return $accessKey;
    }

    /**
     * 发送消息
     * @param $remote_server
     * @param $post_string
     * @return bool|string
     */
    public static function requestByCurl($remote_server, $post_string)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $remote_server);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json;charset=utf-8'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // 线下环境不用开启curl证书验证, 未调通情况可尝试添加该代码
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    /**
     * 屏蔽这些报警
     * @param $url
     * @return bool
     */
    public static function ontInform($url){
        if (empty($url)) {
            return false;
        }
        $urlInfo = pathinfo($url);
        $group = [
            'https://youtu.be',
            'http://twitch.tv',
            'https://twitch.tv',
            'https://www.hltv.org',
            'www.twitch.com',
            'https://lpl.qq.com',
            'https://www.hltv.org',
            'http://www.multitwitch.tv',
            'https://live.bilibili.com',
            'www.twitch.tv',
            'https://www.hltv.org/matches/2349575',
        ];

        foreach ($group as $key => $val) {
            if ($urlInfo['dirname'] == $val) {
                return false;
            }

        }

        $mp = [
            "https://lpl.qq.com/es/live.shtml",
            "https://www.youtube.com/channel/UCXF4WjTCUQSmGapnNEZzbYw",
            'https://live.bilibili.com/31',
            'https://cc.163.com/260825191',
            'https://live.bilibili.com/32',
            'https://www.youtube.com/codleague/live',
            'https://cc.163.com/260825191/',
            "https://www.youtube.com/overwatchleague/live",
            "https://player.twitch.tv/?channel=gamegune&autoplay=true",
            "https://twitch.tv/fadeaway4u",
            "https://twitch.tv/GoTTi1337",
            "https://www.youtube.com/vetv7esports/live",
            "https://twitch.tv/gismooootv",
            "https://twitch.tv/th3josegaming",
            "https://twitch.tv/watchfulTV",
            "https://www.facebook.com/MovistarLPG/live",
            "https://lpl.qq.com/es/live.shtml?bgid=148&bmid=7334",
            "https://twitch.tv/eslswiss",
            "https://twitch.tv/hitpointcz",
            "http://www.youtube.com/codleague/live",
            "https://www.youtube.com/kuyanicwxc/live",
            "https://www.youtube.com/channel/UCbLIqv9Puhyp9_ZjVtfOy7w",
            "https://youtu.be/uamQv2_xWkA",
            "https://youtu.be/ZMrk_yu2wKI",
            "https://www.youtube.com/CODLeague",
            "https://www.hltv.org/matches/2348932/ago-vs-entropiq-spring-sweet-spring-2",
            "https://www.dotabuff.com/matches/6013303490",
            "https://live.bilibili.com/6",
            "www.twitch.com/99damage2",
            "http://twitch.tv/anaty_official",
            "https://twitch.tv/beyondthesummit",
            "https://youtu.be/XV7NZQnQIk4",
        ];
        if (in_array($url,$mp)) {
            return false;
        }
        return true;

    }


    /**
     * 根据条件屏蔽报错
     * @param $url
     * @return bool
     */
    public static function ontInTag($tag){
        if (empty($tag)) {
            return false;
        }

        $mp = [
            "main_increment..team.add.core_data.",
            "event_ws_to_api.5eplay....",
        ];
        if (in_array($tag,$mp)) {
            return false;
        }
        return true;

    }



}