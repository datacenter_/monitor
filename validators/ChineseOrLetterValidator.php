<?php
namespace app\validators;

use yii\validators\RegularExpressionValidator;

/**
 * 中文和字母验证
 * Class ChineseOrWordValidator
 * @package app\validators
 */
class ChineseOrLetterValidator extends RegularExpressionValidator
{
    public $pattern = '/([\x{4e00}-\x{9fa5}A-Za-z]+)/u';

    public $max;

    public $min;

    public $encoding;

    public function init()
    {
        $this->message = null;

        if ($this->message === null) {
            if ($this->max !== null && $this->min !== null) {
                $this->message = \Yii::t('app', '{attribute} only chinese and word, min length {min}, max length {max}');
            } elseif ($this->max !== null) {
                $this->message = \Yii::t('app', '{attribute} only chinese and word, max length {max}');
            } elseif ($this->min !== null) {
                $this->message = \Yii::t('app', '{attribute} only chinese and word, min length {min}');
            } else {
                $this->message = \Yii::t('app', '{attribute} only chinese and word');
            }
        }

        if ($this->encoding === null) {
            $this->encoding = \Yii::$app->charset ? \Yii::$app->charset : 'UTF-8';
        }

        parent::init();
    }

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $length = mb_strlen($model->{$attribute}, $this->encoding);

        if (!preg_match($this->pattern, $model->{$attribute}) || ($this->max !== null && $length > $this->max)) {
            $this->addError($model, $attribute, $this->message, ['max' => $this->max, 'min' => $this->min]);
        }
    }
}