FROM registry-intl.cn-hongkong.aliyuncs.com/bcdc/bk_base:1.8.0
COPY . /var/www/gamecenter
## init auth
RUN chmod -R 777 /var/www/gamecenter/public
RUN chmod -R 777 /var/www/gamecenter/runtime
RUN apk update && \
    apk upgrade && \
    apk add --no-cache bash git openssh
RUN cd /var/www/gamecenter \
&& rm -f composer.lock \
&& composer install
COPY ./game_center.conf /etc/nginx/conf.d/gamecenter.conf
RUN rm -f /etc/nginx/conf.d/default.conf
EXPOSE 80
